plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
    google()
}


kotlinDslPluginOptions {
    experimentalWarning.set(false)
}

// remember to change in Versions.kt
// it is not possible to reference here as they are compiled after this file is compiled
val kotlinVersion = "1.4.31"
val gradleVersion = "4.1.3"

dependencies {
    implementation("com.android.tools.build:gradle:$gradleVersion")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
}
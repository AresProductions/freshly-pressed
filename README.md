# Freshly Pressed

An Android App that lists the most recent stories from WordPress.com Discover.

## Resources

- [Discover REST endpoint][discover].
- [OKHttp][] for networking.

[discover]: https://public-api.wordpress.com/rest/v1.1/sites/discover.wordpress.com/posts/
[OKHttp]: http://square.github.io/okhttp/

## Building

1. Clone the git repository
2. Run `./gradlew build`

## Your Task

Display the "Featured Image" for each post that has one within the list of posts in the app.

* Treat this as if you own the app, and you'll be releasing the finished product and source code under your own name.
* Keep in mind:
    * Android's [Kotlin Style  Guide](https://developer.android.com/kotlin/style-guide) and [Java Style Guide](https://source.android.com/source/code-style.html)
    * i18n: every user visible string should be translatable.
    * Security, best practices, code documentation, etc.
* Don't be code-shy and feel free to explore other parts of the app, not only those you need to touch to solve your problem.
* If you spot any bugs or areas for improvement, please go ahead and make changes to polish up the app.
* Commit changes to the git repository that we provided to you, splitting up commits as necessary. We’d love to see a pull request with your changes.
* Feel free to change anything you want in this repository (project structure, build configuration, README, etc.).
* Let us know when you're done with the test.

Take the time you need, but while there’s no hard time limit most candidates usually spend about 4-6 hours to make their improvements.

# Implementation
### Technologies
* Language: Kotlin
* Core Libraries: Room, Coroutines, Hilt, Coil, OkHttp, Mockk
* Android Target: API 21+

### Architecture
* General Architecture: Clean Architecture
* Presentation Layer: MVVM
* Data Layer: Repository which retrieved from Remote Source and then saves data to Local Source (Database) . The Local Source is the Single Source of Truth and is observed via reactive stream.
* Testing: Unit Testing

### Changes:
* Migrated from Groovy to Kotlin
* Changed Java code to Kotlin
* Upgraded minimum SDK from 15 to 21
* Small redesign of XML views
* Added a ViewModel dependency to the PostListFragment
* Refactored and removed bad code from Fragment
* Added RecyclerView for the list
* Added Dependency Injection via Hilt
* Implemented a Local Source with Room Database
* Implemented a Remote Source with Retrofit - OkHttp
* Implemented Repository Pattern that downloads from remote and saves to local source
* View is updated via a reactive stream (coroutines flow)
* Added Domain Layer (Sync and Get Use Cases)
* Unit Tests for ViewModel, Use Cases and Repository

### Changelog
* feat: post click opens browser (#9)
* tests: unit tests for view-model and adapter (#8)
* tests: unit tests for repository and use cases (#7)
* feat: implement repository and data sources (#6)
* feat: implement view model with mocked use cases (#5)
* feat: refactor fragment and add viewmodel (#3)
* build: migrate from groovy to kotlin (#2)

### What could have been improved?
* More Unit tests
* UI tests
* Basic CI

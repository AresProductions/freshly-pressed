package com.automattic.freshlypressed.domain

import com.automattic.freshlypressed.domain.core.toLocalDate
import com.automattic.freshlypressed.domain.core.toStringDate
import org.junit.Test
import org.threeten.bp.LocalDate

class DateUtilsTest {
    @Test
    fun localDateToString() {
        // given
        val date = LocalDate.of(2000, 7, 14)
        val expectedString = "2000-07-14"
        // then
        assert(date.toStringDate() == expectedString)
    }

    @Test
    fun stringToLocalDate() {
        // given
        val string = "2000-07-14"
        val expectedDate = LocalDate.of(2000, 7, 14)

        // then
        assert(string.toLocalDate() == expectedDate)
    }
}
package com.automattic.freshlypressed.domain

import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.repository.IPostRepository
import com.automattic.freshlypressed.domain.post.usecase.SyncPostsUseCase
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test

class SyncPostsUseCaseTest {

    private val postRepo = mockk<IPostRepository>()
    private val useCase = SyncPostsUseCase(postRepo)

    @ExperimentalCoroutinesApi
    @Test
    fun `given repo sync succeeds, when use case executes, then success is returned`() =
        runBlockingTest {
            // given
            val result = Result.Success(Unit)
            coEvery { postRepo.sync() } returns result

            // when, then
            Assert.assertEquals(useCase.execute(), result)
            coVerify { postRepo.sync() }
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `given repo sync fails, when use case executes, then error is returned`() =
        runBlockingTest {
            // given
            val result = Result.Error(Throwable())
            coEvery { postRepo.sync() } returns result

            // when, then
            Assert.assertEquals(useCase.execute(), result)
            coVerify { postRepo.sync() }
        }

}
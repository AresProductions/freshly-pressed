package com.automattic.freshlypressed.domain

import com.automattic.freshlypressed.domain.post.model.Post
import com.automattic.freshlypressed.domain.post.repository.IPostRepository
import com.automattic.freshlypressed.domain.post.usecase.GetPostsUseCase
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test

class GetPostsUseCaseTest {

    private val postRepo = mockk<IPostRepository>()
    private val useCase = GetPostsUseCase(postRepo)

    @ExperimentalCoroutinesApi
    @Test
    fun `given repo returns a list, when use cases is executed, then the list is returned`() =
        runBlockingTest {
            // given
            val postList = emptyList<Post>()
            every { postRepo.getAll() } returns flowOf(postList)

            // when, then
            useCase.execute().collect {
                Assert.assertEquals(it, postList)
            }
            verify { postRepo.getAll() }
        }

}
package com.automattic.freshlypressed.data

import com.automattic.freshlypressed.data.local.post.IPostLocalSource
import com.automattic.freshlypressed.data.remote.post.IPostRemoteSource
import com.automattic.freshlypressed.data.repository.PostRepository
import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.model.Post
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test
import org.threeten.bp.LocalDate

class PostRepositoryTest {

    private val postRemoteSource = mockk<IPostRemoteSource>()
    private val postLocalSource = mockk<IPostLocalSource>()

    private val repo = PostRepository(postRemoteSource, postLocalSource)

    @ExperimentalCoroutinesApi
    @Test
    fun `given remote source fails, when sync is called, then return error`() = runBlockingTest {
        // given
        val throwable = Throwable()
        val remoteResult = Result.Error(throwable)
        coEvery { postRemoteSource.getPosts() } returns remoteResult

        // when, then
        Assert.assertEquals(repo.sync(), remoteResult)
        coVerify { postRemoteSource.getPosts() }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `given local source fails, when sync is called, then return error`() = runBlockingTest {
        // given
        val remoteResult = Result.Success(emptyList<Post>())
        coEvery { postRemoteSource.getPosts() } returns remoteResult

        val throwable = Throwable()
        val localResult = Result.Error(throwable)

        coEvery { postLocalSource.addPosts(any()) } returns localResult

        // when, then
        Assert.assertEquals(repo.sync(), localResult)
        coVerify { postRemoteSource.getPosts() }
        coVerify { postLocalSource.addPosts(any()) }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `given remote and local source succeeds, when sync is called, then success`() =
        runBlockingTest {
            // given
            val remoteResult = Result.Success(emptyList<Post>())
            coEvery { postRemoteSource.getPosts() } returns remoteResult

            val localResult = Result.Success(Unit)
            coEvery { postLocalSource.addPosts(any()) } returns localResult

            // when, then
            Assert.assertEquals(repo.sync(), Result.Success(Unit))
            coVerify { postRemoteSource.getPosts() }
            coVerify { postLocalSource.addPosts(any()) }
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `given local source, when getAll is called, then return given posts`() = runBlockingTest {
        // given
        val postList =
            listOf(Post(10, "title", "author", "url", "imageUrl", LocalDate.of(2020, 8, 15)))
        every { postLocalSource.getPosts() } returns flowOf(postList)

        // when, then
        repo.getAll().collect {
            Assert.assertEquals(it, postList)
        }
        verify { postLocalSource.getPosts() }
    }

}
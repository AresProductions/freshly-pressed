package com.automattic.freshlypressed.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.automattic.freshlypressed.core.MainCoroutineRule
import com.automattic.freshlypressed.core.emitsValueEventually
import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.model.Post
import com.automattic.freshlypressed.domain.post.usecase.IGetPostsUseCase
import com.automattic.freshlypressed.domain.post.usecase.ISyncPostsUseCase
import com.automattic.freshlypressed.presentation.core.RequestState
import com.automattic.freshlypressed.presentation.posts.PostListViewModel
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.threeten.bp.LocalDate
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class PostListViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutinesRule = MainCoroutineRule()

    private val syncPostsUseCase = mockk<ISyncPostsUseCase>()
    private val getPostsUseCase = mockk<IGetPostsUseCase>()

    @ExperimentalCoroutinesApi
    @Test
    fun `given viewModel initializes, when getPostsUseCase is executed, view model posts live data is updated`() =
        runBlockingTest {
            // given
            val postList = emptyList<Post>()
            every { getPostsUseCase.execute() } returns flowOf(postList)

            // when
            val viewModel = PostListViewModel(syncPostsUseCase, getPostsUseCase)

            // then
            getPostsUseCase.execute().collect {
                MatcherAssert.assertThat(viewModel.posts, emitsValueEventually(postList))
            }
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `given syncPostsUseCase succeeds, when syncPosts is called, request state live data will succeed`() =
        runBlockingTest {
            // given
            every { getPostsUseCase.execute() } returns flowOf(emptyList())
            val viewModel = PostListViewModel(syncPostsUseCase, getPostsUseCase)
            coEvery { syncPostsUseCase.execute() } returns Result.Success(Unit)

            // when
            MatcherAssert.assertThat(
                viewModel.requestState,
                emitsValueEventually(RequestState.IDLE)
            )
            viewModel.syncPosts()

            // then
            MatcherAssert.assertThat(
                viewModel.requestState,
                emitsValueEventually(RequestState.SUCCESS)
            )
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `given syncPostsUseCase fails, when syncPosts is called, request state live data will fail`() =
        runBlockingTest {
            // given
            every { getPostsUseCase.execute() } returns flowOf(emptyList())
            val viewModel = PostListViewModel(syncPostsUseCase, getPostsUseCase)
            coEvery { syncPostsUseCase.execute() } returns Result.Error(Throwable())

            // when
            MatcherAssert.assertThat(
                viewModel.requestState,
                emitsValueEventually(RequestState.IDLE)
            )
            viewModel.syncPosts()

            // then
            MatcherAssert.assertThat(
                viewModel.requestState,
                emitsValueEventually(RequestState.FAIL)
            )
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `given post id, when getPostUrl is called, return the url of the post or null if not found`() =
        runBlockingTest {
            // given
            val post1 = Post(0, "", "", "url", "", LocalDate.of(2000, 10, 12))
            val post2 = Post(1, "", "", "url2", "", LocalDate.of(2000, 10, 12))
            val postList = listOf(post1, post2)
            every { getPostsUseCase.execute() } returns flowOf(postList)
            val viewModel = PostListViewModel(syncPostsUseCase, getPostsUseCase)

            // when
            getPostsUseCase.execute().collect {
                MatcherAssert.assertThat(viewModel.posts, emitsValueEventually(postList))
                // then
                Assert.assertEquals("url", viewModel.getPostUrl("0"))
                Assert.assertNull(viewModel.getPostUrl(""))
            }
        }
}

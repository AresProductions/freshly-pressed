package com.automattic.freshlypressed.presentation

import com.automattic.freshlypressed.domain.core.toStringDate
import com.automattic.freshlypressed.domain.post.model.Post
import com.automattic.freshlypressed.presentation.posts.PostItem
import com.automattic.freshlypressed.presentation.posts.mapToAdapterPostItems
import org.junit.Assert
import org.junit.Test
import org.threeten.bp.LocalDate

class PostItemTests {

    @Test
    fun `given list of posts, when mapToAdapterPostItems is called, return expected post items`() {
        // given
        val posts = listOf(
            Post(0, "title", "author", "", "", LocalDate.of(2000, 10, 12)),
            Post(1, "title2", "author2", "", "", LocalDate.of(2000, 10, 10)),
            Post(2, "title3", "author3", "", "", LocalDate.of(2000, 10, 11)),
            Post(3, "title4", "author", "", "", LocalDate.of(2000, 10, 10))
        )

        val expectedItems = listOf(
            PostItem.Header(posts[0].date.toStringDate()),
            PostItem.Child(posts[0]),
            PostItem.Header(posts[2].date.toStringDate()),
            PostItem.Child(posts[2]),
            PostItem.Header(posts[1].date.toStringDate()),
            PostItem.Child(posts[1]),
            PostItem.Child(posts[3])
        )

        // when, then
        Assert.assertEquals(posts.mapToAdapterPostItems(), expectedItems)
    }
}
package com.automattic.freshlypressed.presentation.posts

import android.view.View
import coil.load
import com.automattic.freshlypressed.R
import com.automattic.freshlypressed.domain.core.toStringDate
import com.automattic.freshlypressed.domain.post.model.Post
import com.automattic.freshlypressed.presentation.core.BindableAdapterItem
import com.automattic.freshlypressed.presentation.core.BindableRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_post.view.*
import kotlinx.android.synthetic.main.item_post_header.view.*

class PostsAdapter : BindableRecyclerViewAdapter<PostItem>() {

    var postClickListener: ((PostItem.Child) -> Unit)? = null

    override fun onClick(view: View, item: PostItem) {
        when (item) {
            is PostItem.Child -> postClickListener?.invoke(item)
            else -> Unit
        }
    }
}

sealed class PostItem : BindableAdapterItem {

    data class Header(val date: String) : PostItem() {

        override val id: String = date

        override fun isClickable() = false

        override fun getLayoutId() = R.layout.item_post_header

        override fun bind(view: View) {
            view.text_header.text = date
        }
    }

    data class Child(val post: Post) : PostItem() {

        override val id: String = post.id.toString()

        override fun isClickable() = true

        override fun getLayoutId() = R.layout.item_post

        override fun bind(view: View) {
            view.text_title.text = post.title
            view.text_author.text = post.author
            view.image_post.load(post.imageUrl) {
                placeholder(R.drawable.ic_placeholder)
                crossfade(true)
            }
        }
    }

}

fun List<Post>.mapToAdapterPostItems(): List<PostItem> =
    groupBy { it.date.toStringDate() }
        .toSortedMap(reverseOrder())
        .flatMap { (localDate, posts) ->
            val headerItem = PostItem.Header(localDate)
            val postItems = posts.map { PostItem.Child(it) }

            listOf(headerItem) + postItems
        }

package com.automattic.freshlypressed.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.automattic.freshlypressed.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

package com.automattic.freshlypressed.presentation.core

enum class RequestState {
    IN_PROGRESS,
    SUCCESS,
    FAIL,
    IDLE
}
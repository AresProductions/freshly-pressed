package com.automattic.freshlypressed.presentation.core

import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

/**
 * Default [RecyclerView.Adapter].
 * - Reduces configuration complexity.
 * - Supports multiple items (group, child).
 */
abstract class BindableRecyclerViewAdapter<T : BindableAdapterItem> :
        ListAdapter<T, RecyclerView.ViewHolder>(BindableAdapterItemDiffCallback<T>()) {

    override fun getItemViewType(position: Int) = currentList[position].getLayoutId()

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewTypeAsLayoutId: Int
    ): RecyclerView.ViewHolder {
        val view = parent.inflate(viewTypeAsLayoutId, attachToRoot = false)

        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = currentList[position]
        val view = holder.itemView

        onBindItemView(item, view)

        view.setOnClickListener(
                if (item.isClickable()) {
                    View.OnClickListener { onClick(it, item) }
                } else {
                    null
                }
        )
    }

    @CallSuper
    protected open fun onBindItemView(item: T, view: View) {
        item.bind(view)
    }

    /**
     * Default [RecyclerView.ViewHolder]. No implementation detail required for item <-> view binding here.
     */
    private class RecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view)

    open fun onClick(view: View, item: T) {} // empty standard implementation
}

/**
 * Required item -> view binding interface for [BindableRecyclerViewAdapter] items.
 */
interface BindableAdapterItem {

    /**
     * Id to be used to identify same objects (with potentially different data)
     */
    val id: String

    /**
     * Defines layout by id for this item (used as view type for recycler view)
     */
    @LayoutRes
    fun getLayoutId(): Int

    /**
     * Will be called if item is attached the a new view holder.
     *
     * Should be used to set all child views to its target state based on item data.
     */
    fun bind(view: View)

    /**
     * Indicates if view is clickable
     */
    fun isClickable(): Boolean
}

private class BindableAdapterItemDiffCallback<T : BindableAdapterItem> :
        DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(
            oldItem: T,
            newItem: T
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: T,
            newItem: T
    ): Boolean {
        return oldItem.hashCode() == newItem.hashCode()
    }

}

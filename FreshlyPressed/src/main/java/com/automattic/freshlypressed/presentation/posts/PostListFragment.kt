package com.automattic.freshlypressed.presentation.posts

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.automattic.freshlypressed.R
import com.automattic.freshlypressed.domain.post.model.Post
import com.automattic.freshlypressed.presentation.core.IntentUtils
import com.automattic.freshlypressed.presentation.core.RequestState
import com.automattic.freshlypressed.presentation.core.observe
import com.automattic.freshlypressed.presentation.core.skipNull
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_post_list.*

@AndroidEntryPoint
class PostListFragment : Fragment(R.layout.fragment_post_list) {

    private val viewModel: PostListViewModel by viewModels()

    private val postsAdapter = PostsAdapter()

    // region lifecycle methods

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupSwipeRefresh()

        viewModel.run {
            resetState()
            observe(posts, ::handlePosts.skipNull())
            observe(requestState, ::handleRequestState.skipNull())
            if (savedInstanceState == null) {
                syncPosts()
            }
        }
    }

    // endregion

    // region private functions

    private fun setupRecyclerView() {
        recycler_view_posts.adapter = postsAdapter
        recycler_view_posts.layoutManager = LinearLayoutManager(context)

        postsAdapter.apply {
            postClickListener = ::onPostClicked
        }
    }

    private fun onPostClicked(post: PostItem.Child) {
        viewModel.getPostUrl(post.id)?.let {
            IntentUtils.openUrlWithBrowser(requireContext(), it)
            return
        }

        Toast.makeText(
            requireContext(),
            R.string.no_url_available,
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun setupSwipeRefresh() {
        swipe_refresh.setOnRefreshListener {
            viewModel.syncPosts()
        }
    }

    private fun handlePosts(posts: List<Post>) {
        postsAdapter.submitList(posts.mapToAdapterPostItems())
    }

    private fun handleRequestState(requestState: RequestState) {
        showToastForRequestState(requestState)
        showSwipeAnimationForRequestState(requestState)
    }

    private fun showSwipeAnimationForRequestState(requestState: RequestState) {
        swipe_refresh.isRefreshing = requestState == RequestState.IN_PROGRESS
    }

    private fun showToastForRequestState(requestState: RequestState) {
        when (requestState) {
            RequestState.SUCCESS -> Toast.makeText(
                requireContext(),
                R.string.sync_success,
                Toast.LENGTH_SHORT
            ).show()
            RequestState.FAIL -> Toast.makeText(
                requireContext(),
                R.string.sync_fail,
                Toast.LENGTH_SHORT
            ).show()
            else -> Unit
        }
    }

    // endregion

}
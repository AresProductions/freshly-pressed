package com.automattic.freshlypressed.presentation.core

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

/**
 * Inflates the given layout and attaches it to this ViewGroup by default.
 */
fun ViewGroup.inflate(@LayoutRes layoutResId: Int, attachToRoot: Boolean = true): View {
    return LayoutInflater.from(context).inflate(layoutResId, this, attachToRoot)
}
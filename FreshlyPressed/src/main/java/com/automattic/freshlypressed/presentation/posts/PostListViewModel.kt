package com.automattic.freshlypressed.presentation.posts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.usecase.IGetPostsUseCase
import com.automattic.freshlypressed.domain.post.usecase.ISyncPostsUseCase
import com.automattic.freshlypressed.presentation.core.RequestState
import com.automattic.freshlypressed.presentation.core.immutable
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PostListViewModel @Inject constructor(
    private val syncPostsUseCase: ISyncPostsUseCase,
    getPostsUseCase: IGetPostsUseCase
) : ViewModel() {

    private val _requestState = MutableLiveData(RequestState.IDLE)
    val requestState = _requestState.immutable()

    val posts = getPostsUseCase.execute().asLiveData()

    fun resetState() {
        _requestState.value = RequestState.IDLE
    }

    fun syncPosts() {
        viewModelScope.launch(Dispatchers.IO) {
            _requestState.postValue(RequestState.IN_PROGRESS)
            when (val response = syncPostsUseCase.execute()) {
                is Result.Success -> {
                    _requestState.postValue(RequestState.SUCCESS)
                }
                is Result.Error -> {
                    Timber.e(response.throwable)
                    _requestState.postValue(RequestState.FAIL)
                }
            }
        }
    }

    fun getPostUrl(id: String): String? {
        return posts.value?.let { postList ->
            postList.firstOrNull { it.id.toString() == id }
        }?.url
    }

}
package com.automattic.freshlypressed.presentation.core

import android.content.Context
import android.content.Intent
import android.net.Uri

object IntentUtils {
    fun openUrlWithBrowser(context: Context, url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }
}
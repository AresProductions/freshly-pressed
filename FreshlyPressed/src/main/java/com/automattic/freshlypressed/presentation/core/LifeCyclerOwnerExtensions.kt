package com.automattic.freshlypressed.presentation.core

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * In the context of a [LifecycleOwner]
 * When [LiveData] observer hits, the given [body] will be executed with its value as param
 */
fun <T : Any?, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) =
    liveData.observe(this, { body(it) })

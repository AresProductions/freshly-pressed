package com.automattic.freshlypressed.presentation.core

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Transform [MutableLiveData] to immutable [LiveData]
 */
fun <T> MutableLiveData<T>.immutable(): LiveData<T> = this
package com.automattic.freshlypressed.data.local.core

import android.content.Context
import androidx.room.Room
import com.automattic.freshlypressed.data.local.post.DPostDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun providePostDao(database: FreshlyPressedDatabase): DPostDao {
        return database.postDao()
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): FreshlyPressedDatabase {
        return Room.databaseBuilder(
            appContext,
            FreshlyPressedDatabase::class.java,
            FreshlyPressedDatabase.DATABASE_NAME
        ).build()
    }
}
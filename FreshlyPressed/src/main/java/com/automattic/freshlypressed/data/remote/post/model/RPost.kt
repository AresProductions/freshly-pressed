package com.automattic.freshlypressed.data.remote.post.model

import com.automattic.freshlypressed.domain.core.toLocalDate
import com.automattic.freshlypressed.domain.post.model.Post
import com.google.gson.annotations.SerializedName

data class RPost(
    @SerializedName("ID")
    val id: Int,
    val title: String,
    @SerializedName("URL")
    val url: String,
    val date: String,
    val author: RAuthor,
    val featured_image: String
)

fun RPost.toDomain() = Post(
    id = id,
    title = title,
    url = url,
    date = date.substring(0, 10).toLocalDate(),
    author = author.name,
    imageUrl = featured_image
)

package com.automattic.freshlypressed.data.repository

import com.automattic.freshlypressed.data.local.post.IPostLocalSource
import com.automattic.freshlypressed.data.local.post.PostLocalSource
import com.automattic.freshlypressed.data.remote.post.IPostRemoteSource
import com.automattic.freshlypressed.data.remote.post.PostRemoteSource
import com.automattic.freshlypressed.domain.post.repository.IPostRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface RepositoryModule {
    @get:Binds
    val PostRepository.iPostRepo: IPostRepository

    @get:Binds
    val PostRemoteSource.iPostRemoteSource: IPostRemoteSource

    @get:Binds
    val PostLocalSource.iPostLocalSource: IPostLocalSource
}
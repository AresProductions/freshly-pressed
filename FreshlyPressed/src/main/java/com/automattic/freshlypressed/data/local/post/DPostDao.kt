package com.automattic.freshlypressed.data.local.post

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.automattic.freshlypressed.data.local.post.model.DPost
import kotlinx.coroutines.flow.Flow

@Dao
interface DPostDao {
    @Query("SELECT * FROM posts")
    fun getAll(): Flow<List<DPost>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posts: List<DPost>)
}
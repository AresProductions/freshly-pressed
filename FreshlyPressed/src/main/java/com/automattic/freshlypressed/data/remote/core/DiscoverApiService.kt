package com.automattic.freshlypressed.data.remote.core

import com.automattic.freshlypressed.data.remote.post.model.PostResponse
import retrofit2.http.GET

interface DiscoverApiService {
    companion object {
        const val BASE_URL =
            "https://public-api.wordpress.com/rest/v1.1/sites/discover.wordpress.com/"
    }

    @GET("posts")
    suspend fun getPosts(): PostResponse

}
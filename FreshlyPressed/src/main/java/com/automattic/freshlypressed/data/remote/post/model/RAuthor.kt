package com.automattic.freshlypressed.data.remote.post.model

data class RAuthor(
    val name: String
)

package com.automattic.freshlypressed.data.local.post.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.automattic.freshlypressed.domain.core.toLocalDate
import com.automattic.freshlypressed.domain.core.toStringDate
import com.automattic.freshlypressed.domain.post.model.Post

@Entity(tableName = "posts")
data class DPost(
    @PrimaryKey
    @ColumnInfo
    val id: Int,
    @ColumnInfo
    val title: String,
    @ColumnInfo
    val author: String,
    @ColumnInfo
    val url: String,
    @ColumnInfo
    val imageUrl: String,
    @ColumnInfo
    val date: String
)

fun DPost.toDomain() = Post(id, title, author, url, imageUrl, date.toLocalDate())

fun Post.toData() = DPost(id, title, author, url, imageUrl, date.toStringDate())
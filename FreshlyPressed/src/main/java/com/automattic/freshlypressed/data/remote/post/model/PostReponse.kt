package com.automattic.freshlypressed.data.remote.post.model

data class PostResponse(
    val posts: List<RPost>
)
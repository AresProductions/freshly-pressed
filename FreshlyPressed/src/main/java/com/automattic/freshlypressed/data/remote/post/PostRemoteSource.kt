package com.automattic.freshlypressed.data.remote.post

import com.automattic.freshlypressed.data.remote.core.DiscoverApiService
import com.automattic.freshlypressed.data.remote.post.model.toDomain
import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.model.Post
import javax.inject.Inject

class PostRemoteSource @Inject constructor(
    private val api: DiscoverApiService
) : IPostRemoteSource {
    override suspend fun getPosts(): Result<List<Post>> {
        return try {
            Result.Success(api.getPosts().posts.map { it.toDomain() })
        } catch (t: Throwable) {
            Result.Error(t)
        }
    }
}

interface IPostRemoteSource {
    suspend fun getPosts(): Result<List<Post>>
}
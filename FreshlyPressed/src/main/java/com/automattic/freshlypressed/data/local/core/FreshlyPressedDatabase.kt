package com.automattic.freshlypressed.data.local.core

import androidx.room.Database
import androidx.room.RoomDatabase
import com.automattic.freshlypressed.data.local.post.DPostDao
import com.automattic.freshlypressed.data.local.post.model.DPost

@Database(entities = [DPost::class], version = 1, exportSchema = false)
abstract class FreshlyPressedDatabase : RoomDatabase() {
    abstract fun postDao(): DPostDao

    companion object {
        const val DATABASE_NAME = "freshly-pressed.db"
    }
}
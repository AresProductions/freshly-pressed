package com.automattic.freshlypressed.data.repository

import com.automattic.freshlypressed.data.local.post.IPostLocalSource
import com.automattic.freshlypressed.data.remote.post.IPostRemoteSource
import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.model.Post
import com.automattic.freshlypressed.domain.post.repository.IPostRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val postRemoteSource: IPostRemoteSource,
    private val postLocalSource: IPostLocalSource
) : IPostRepository {
    override suspend fun sync(): Result<Unit> {
        return when (val response = postRemoteSource.getPosts()) {
            is Result.Success -> {
                postLocalSource.addPosts(response.data)
            }
            is Result.Error -> {
                return Result.Error(response.throwable)
            }
        }
    }

    override fun getAll(): Flow<List<Post>> {
        return postLocalSource.getPosts()
    }
}
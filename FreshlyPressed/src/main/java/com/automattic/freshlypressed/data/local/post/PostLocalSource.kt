package com.automattic.freshlypressed.data.local.post

import com.automattic.freshlypressed.data.local.post.model.toData
import com.automattic.freshlypressed.data.local.post.model.toDomain
import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.model.Post
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PostLocalSource @Inject constructor(
    private val postDao: DPostDao
) : IPostLocalSource {

    override suspend fun addPosts(posts: List<Post>): Result<Unit> {
        return try {
            Result.Success(postDao.insertAll(posts.map { it.toData() }))
        } catch (t: Throwable) {
            Result.Error(t)
        }
    }

    override fun getPosts(): Flow<List<Post>> {
        return postDao.getAll().map { dPosts ->
            dPosts.map { it.toDomain() }
        }
    }

}

interface IPostLocalSource {
    suspend fun addPosts(posts: List<Post>): Result<Unit>

    fun getPosts(): Flow<List<Post>>
}
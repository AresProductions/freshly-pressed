package com.automattic.freshlypressed.domain

import com.automattic.freshlypressed.domain.post.usecase.GetPostsUseCase
import com.automattic.freshlypressed.domain.post.usecase.IGetPostsUseCase
import com.automattic.freshlypressed.domain.post.usecase.ISyncPostsUseCase
import com.automattic.freshlypressed.domain.post.usecase.SyncPostsUseCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface UseCaseModule {
    @get:Binds
    val GetPostsUseCase.iGetPostsUseCase: IGetPostsUseCase

    @get:Binds
    val SyncPostsUseCase.iSyncPostsUseCase: ISyncPostsUseCase
}
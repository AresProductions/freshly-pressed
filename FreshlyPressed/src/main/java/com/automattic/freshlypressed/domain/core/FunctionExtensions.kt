package com.automattic.freshlypressed.presentation.core

/**
 * Converts a function with a non-null parameter to a function with a nullable parameter.
 * In case of a given null parameter, the function will not be executed (skipped).
 */
fun <Param> ((Param) -> Unit).skipNull(): (Param?) -> Unit = { param ->
    if (param != null) {
        invoke(param)
    }
}

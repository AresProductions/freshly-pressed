package com.automattic.freshlypressed.domain.post.usecase

import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.repository.IPostRepository
import javax.inject.Inject

interface ISyncPostsUseCase {
    suspend fun execute(): Result<Unit>
}

class SyncPostsUseCase @Inject constructor(
    private val postRepository: IPostRepository
) : ISyncPostsUseCase {
    override suspend fun execute(): Result<Unit> = postRepository.sync()
}

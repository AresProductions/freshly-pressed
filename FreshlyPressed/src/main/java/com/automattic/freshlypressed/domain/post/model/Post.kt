package com.automattic.freshlypressed.domain.post.model

import org.threeten.bp.LocalDate

data class Post(
        val id: Int,
        val title: String,
        val author: String,
        val url: String,
        val imageUrl: String,
        val date: LocalDate
)
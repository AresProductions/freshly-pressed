package com.automattic.freshlypressed.domain.post.repository

import com.automattic.freshlypressed.domain.core.Result
import com.automattic.freshlypressed.domain.post.model.Post
import kotlinx.coroutines.flow.Flow

interface IPostRepository {
    suspend fun sync(): Result<Unit>
    fun getAll(): Flow<List<Post>>
}
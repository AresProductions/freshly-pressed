package com.automattic.freshlypressed.domain.post.usecase

import com.automattic.freshlypressed.domain.post.model.Post
import com.automattic.freshlypressed.domain.post.repository.IPostRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


interface IGetPostsUseCase {
    fun execute(): Flow<List<Post>>
}

class GetPostsUseCase @Inject constructor(
    private val postRepository: IPostRepository
) : IGetPostsUseCase {
    override fun execute(): Flow<List<Post>> = postRepository.getAll()
}